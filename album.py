# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 18:31:30 2016

@author: etudiant
"""

import logging
logger = logging.getLogger("album")
logger.setLevel(logging.DEBUG)

class Album(object):
    def __init__(self):
        logger.debug("Instanciation d'un genre")
        self.name = str()
        self.year = str()
        self.nTracks = int()
        self.recordCompany = str()
        self.listTracks
        
    def getName(self):
        """Return type name as str()
        """
        pass
    
    def setName(self, _name):
        """Set album name using the given paramater str()
        """
        old_name = self.name
        self.name = str(_name)
        logger.debug("Type name is now : '" + self.name + "'(was : '" + old_name + "')")
    
    def getYear(self):
        """Return year as int()
        """
        pass
    
    def setYear(self, _year):
        """Set year using the given parameter int()
        """
        old_year = self.year
        self.year = str(_year)
        logger.debug("Year is now : '" + self.year + "'(was : '" + old_year + "')")
        
    def getNTracks(self):
        """Return number of tracks as int()
        """
        pass
    
    def setNTracks(self, _nTrack):
        """Set number of tracks using the given parameter int()
        """
        old_nTrack = self.nTrack
        self.nTrack = str(_nTrack)
        logger.debug("Year is now : '" + self.nTrack + "'(was : '" + old_nTrack + "')")
              
    def getRecordCompany(self):
        """Return record company as str()
        """
        pass
    
    def setRecordCompany(self, _recordCompany):
        """Set number of tracks using the given parameter str()
        """
        old_recordCompany = self.recordCompany
        self.recordCompany = str(_recordCompany)
        logger.debug("Year is now : '" + self.recordCompany + "'(was : '" + old_recordCompany + "')")
       
        
if __name__ == "__main__":
    logger.debug("Execution du module 'Album'")
    t = Album()
    logger.debug("Fin d'éxécution du module 'Album'")
else:
    logger.debug("Chargement du module 'Album'")
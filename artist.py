# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 16:00:17 2016

@author: etudiant
"""

import logging
logger = logging.getLogger("artist")
logger.setLevel(logging.DEBUG)

class Artist(object):
    def __init__(self):
        logger.debug("Instanciation d'un 'Artiste'")
        self.name = str()
        self.sex = str()
        self.beginningYear = int()
        self.label = str()
        
    def getName(self):
        """Return artist name as a str()
        """
        pass
    
    def setName(self, _name):
        """Set artist name using the given parameter str()
        """
        old_name = self.name
        self.name = str(_name)
        logger.debug("Artist name is now : '" + self.name + "'(was : '" + old_name + "')")
        
    def getSex(self):
        """Return artist sex as a str()
        """
        pass
    
    def setSex(self, _sex):
        """Set artist sex using the given parameter str()
        """
        old_sex = self.sex
        self.sex = str(_sex)
        logger.debug("Artist sex is now : '" + self.sex + "'(was : '" + old_sex + "')")
        
    def getBeginningYear(self):
        """Return artist beginning year as a int()
        """
        pass
    
    def setBeginningYear(self, _beginningYear):
        """Set artist beginning year using the given parameter int()
        """
        old_beginningYear = self.beginningYear
        self.beginningYear = str(_beginningYear)
        logger.debug("Artist beginning year is now : '" + self.beginningYear + "'(was : '" + old_beginningYear + "')")
        
    def getRecordingCompany(self):
        """Return artist label as a str()
        """
        pass
    
    def setRecordingCompany(self, _label):
        """Set artist label using the given parameter str()
        """
        old_label = self.label
        self.sex = str(_label)
        logger.debug("Artist label is now : '" + self.label + "'(was : '" + old_label + "')")
        
if __name__ == "__main__":
    logger.debug("Execution du module 'Artist'")
    t = Artist()
    logger.debug("Fin d'éxécution du module 'Artist'")
else:
    logger.debug("Chargement du module 'Artist'")
        
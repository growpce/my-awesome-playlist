# -*- coding: utf-8 -*-
"""
Created on Tue Nov 15 16:46:01 2016

@author: etudiant
"""

import track

class Exporter():
    
    
    def m3u (playlist):
        print("Format M3U")
        fichier = open("playlist.m3u","w")
        for currentTrack in playlist:
            fichier.write(currentTrack.path+"\n")
            #print(currentTrack.nId, " - ", currentTrack.title, " - ", currentTrack.artist, " - ", currentTrack.genre )
            currentTrack.Afficher()
        fichier.close()
        
    def xspf (playlist):
        print("Format XPSF")
        fichier = open("playlist.xspf","w")
        fichier.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<playlist version=\"1\" xmlns=\"http://xspf.org/ns/0/\">\n\t<tracklist>")
        for currentTrack in playlist:
            fichier.write("\n\t\t<track><location>file://"+currentTrack.path+"</location></track>")
            currentTrack.Afficher()            
            #print(currentTrack.nId, " - ", currentTrack.title, " - ", currentTrack.artist, " - ", currentTrack.genre )
        fichier.write("\n\t</tracklist>\n</playlist>")
        fichier.close()

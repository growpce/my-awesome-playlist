# -*- coding: utf-8 -*-
"""
Created on Wed Nov  9 15:22:29 2016

@author: etudiant
"""
    
import psycopg2
import logging
import argparse
import track
import export

logger = logging.getLogger("generateur")
logger.setLevel(logging.DEBUG)

def main():
    logger.debug("Entrée dans 'main'")
    
    playlist = []
    tracklist = []
    pTime = 0;    
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--time", dest = "time", type=int, default = 5)
    parser.add_argument("-g", "--genre", dest = "genre", nargs = 2, action='append')
    parser.add_argument("-a", "--artist", dest = "artist", nargs = 2, action='append')
    parser.add_argument("--xspf", dest = "xspf", action = "store_true")
    args = parser.parse_args()
    
    tTime = args.time*60
    
    
    #Récupération des 'Tracks'
    connexion = psycopg2.connect(database="cpichereau", user="c.pichereau", password="px3mrjh2", host="172.16.99.2", port="5432")
    curseur = connexion.cursor()
    curseur.execute("SELECT * FROM playlist.track")
    trackInfo = curseur.fetchall()
    
    #Création objets 'Tracks'
    for currentTrack in trackInfo:
        piste = track.Track(currentTrack[0],currentTrack[1],currentTrack[2],currentTrack[3],currentTrack[4],currentTrack[5],currentTrack[6])
        tracklist.append(piste)
    
    
    #Recherche des 'Tracks' par artiste                    
    if args.artist:
        for artist in args.artist:
            aTime = 0
            for currentTrack in playlist:
                if currentTrack.artist == artist:
                    aTime = aTime + currentTrack.duration
            for currentTrack in tracklist:
                if aTime > tTime*(int(artist[1])/100):
                    break
                else:
                    if not currentTrack in playlist and currentTrack.artist == artist[0]:
                        playlist.append(currentTrack)
                        pTime = pTime + currentTrack.duration
                        aTime = aTime + currentTrack.duration
                        
        
    #Recherche des 'Tracks' par genre   
    if args.genre:
        for genre in args.genre:
            gTime = 0
            for currentTrack in playlist:
                if currentTrack.genre == genre:
                    gTime = gTime + currentTrack.duration
            for currentTrack in tracklist:
                if gTime > tTime*(int(genre[1])/100):
                    break
                else:
                    if not currentTrack in playlist and currentTrack.genre == genre[0]:
                        playlist.append(currentTrack)
                        pTime = pTime + currentTrack.duration
                        gTime = gTime + currentTrack.duration
    
    
    #Remplissage de la playlist si temps restant
    while pTime < tTime:
        curseur.execute("SELECT * FROM playlist.track ORDER BY RANDOM() LIMIT 1")
        trackinfo = curseur.fetchall()
        for currentTrack in trackinfo :
            piste = track.Track(currentTrack[0],currentTrack[1],currentTrack[2],currentTrack[3],currentTrack[4],currentTrack[5],currentTrack[6])
            playlist.append(piste)
            pTime = pTime + piste.duration                         
       # for currentTrack in tracklist:
        #    if pTime > tTime:
         #       break
          #  else:
           #     playlist.append(currentTrack)
            #    pTime = pTime + currentTrack.duration
                
    #Affichage et création fichier playlist
    if args.xspf == True:
        export.Exporter.xspf(playlist)
    else:
        export.Exporter.m3u(playlist)
    
    logger.info("Sortie de 'main'")
    
    
if __name__ == "__main__":
    logger.debug("Exécution du module 'generateur'")
    main()
    logger.debug("Fin d'éxécution du module 'generateur'")
else:
    logger.debug("Chargement du module 'generateur'")
    
    

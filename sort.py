# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 16:47:44 2016

@author: etudiant
"""

import logging
logger = logging.getLogger("sort")
logger.setLevel(logging.DEBUG)

class Type(object):
    def __init__(self):
        logger.debug("Instanciation d'un genre")
        self.name = str()
        
    def getName(self):
        """Return type name as str()
        """
        pass
    
    def setName(self, _name):
        """Set type name using the given paramater str()
        """
        old_name = self.name
        self.name = str(_name)
        logger.debug("Type name is now : '" + self.name + "'(was : '" + old_name + "')")
        
if __name__ == "__main__":
    logger.debug("Execution du module 'Type'")
    t = Type()
    logger.debug("Fin d'éxécution du module 'Type'")
else:
    logger.debug("Chargement du module 'Type'")
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 16:27:23 2016

@author: etudiant
"""

import logging

logger = logging.getLogger("track")
logger.setLevel(logging.DEBUG)

class Track(object):
    def __init__(self):
        logger.debug("Instanciation d'un 'Track'")
        self.nId = int()
        self.title = str()
        self.artist = str()
        self.album = str()
        self.duration = int()
        self.genre = str()
        self.path = str()
        
    def __init__(self, nId, title, artist, album, duration, genre, path):
        logger.debug("Instanciation d'un 'Track'")
        self.nId = nId
        self.title = title
        self.artist = artist
        self.album = album
        self.duration = duration
        self.genre = genre
        self.path = path

    def getTitle(self):
        """Return track title as a str()
        """
        pass
    
    def setTitle(self, _title):
        """Set track title using the given parameter str()
        """
        old_title = self.title
        self.title = str(_title)
        logger.debug("Track title is now : '" + self.title + "'(was : '" + old_title + "')")
        
    def getDuration(self):
        """Return track duration as a int()
        """
        pass        
    
    def setDuration(self, _duration):
        old_duration = self.duration
        self.duration = int(_duration)
        logger.debug("Track duration is now : '" + self.duration + "'(was : '" + old_duration + "')")

    def getPath(self):
        """Return track path and filename as a str()
        """
        pass
    
    def setPath(self, _path):
        """Set track path and filename using the given parameter str()
        """
        old_path = self.path
        self.path = str(_path)
        logger.debug("Track path and filename are now : '" + self.path + "'(was : '" + old_path + "')")
        
    def Afficher(self):
        print(str(self.nId)+" - "+self.title+" - "+self.artist+" - "+self.genre+" - "+str(self.duration))
        
if __name__ == "__main__":
    logger.debug("Execution du module 'Track'")
    t = Track()
    logger.debug("Fin d'éxécution du module 'Track'")
else:
    logger.debug("Chargement du module 'Track'")